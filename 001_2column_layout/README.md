# 二段組のレイアウト

2018-01-02

- これだけで1時間くらいかかってしまう
- MaterializeのNavbarのテキストカラーの指定方法でつまづいた
    ```css
    nav ul a {
        color: black;
    }
    .brand-logo {
        color: black;
    }
    ```
    
    - 両方を指定しなければ反映されなかった
    - `text-black`を`<nav>`に指定してもだめだった

- 配色が難しい

